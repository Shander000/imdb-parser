package shander.imdbparser.ui.fragment;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import shander.imdbparser.R;
import shander.imdbparser.entities.MovieEntityFull;
import shander.imdbparser.ui.view.ExpandableTextView;
import shander.imdbparser.utils.loader.FullMovieInfoLoader;

public class MovieDetailsFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<MovieEntityFull>{

    private static final String IMDB_ID = "mIMDBId";
    private String mIMDBId;
    private Loader<MovieEntityFull> mMovieEntityFullLoader;

    @BindView(R.id.info_error_tv)
    TextView mInfoErrorText;

    @BindView(R.id.info_no_internet_view)
    View mNoInternetView;

    @BindView(R.id.settings_btn)
    Button mSettingsButton;

    @BindView(R.id.movie_info_lay)
    View mMovieView;

    @BindView(R.id.poster_iv)
    ImageView mPosterImage;

    @BindView(R.id.rating_bar)
    RatingBar mRatingBar;

    @BindView(R.id.title_tv)
    TextView mTitleText;

    @BindView(R.id.rating_tv)
    TextView mRatingText;

    @BindView(R.id.country_tv)
    TextView mCountryText;

    @BindView(R.id.producer_tv)
    TextView mProducerText;

    @BindView(R.id.actors_list_tv)
    ExpandableTextView mActorsExpandableText;

    @BindView(R.id.plot_tv)
    TextView mPlotText;


    public MovieDetailsFragment() {
    }

    public static MovieDetailsFragment newInstance(String id){
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        Bundle args = new Bundle();
        args.putString(IMDB_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mIMDBId = getArguments().getString(IMDB_ID);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSettingsButton.setOnClickListener(view -> startActivity(new Intent(Settings.ACTION_SETTINGS)));
        if (mMovieEntityFullLoader == null) {
            mMovieEntityFullLoader = getActivity().getLoaderManager().initLoader(2, null, this);
        } else {
            getActivity().getLoaderManager().restartLoader(2, null, this);
        }
    }

    @NonNull
    @Override
    public Loader<MovieEntityFull> onCreateLoader(int id, @Nullable Bundle args) {
        mMovieEntityFullLoader = new FullMovieInfoLoader(getActivity(), mIMDBId);
        return mMovieEntityFullLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<MovieEntityFull> loader, MovieEntityFull data) {
        if (data.getResponse().equals("True")) {
            fillInfo(data);
        } else {
            showError(data.getError());
        }
        getActivity().getLoaderManager().destroyLoader(2);
    }

    private void showError(String error) {
        mMovieView.setVisibility(View.GONE);
        mInfoErrorText.setVisibility(View.VISIBLE);
        mInfoErrorText.setText(getResources().getString(R.string.error, error));
    }

    private void fillInfo(MovieEntityFull entityFull) {
        Picasso.get().load(entityFull.getPoster()).placeholder(R.drawable.no_poster_available).into(mPosterImage);
        mTitleText.setText(entityFull.getTitle());
        mRatingText.setText(getResources().getString(R.string.imdb_rate, entityFull.getImdbRating()));
        mCountryText.setText(getResources().getString(R.string.country, entityFull.getCountry()));
        mProducerText.setText(getResources().getString(R.string.producer, entityFull.getDirector()));
        mActorsExpandableText.setText(getResources().getString(R.string.actors, entityFull.getActors()));
        mActorsExpandableText.setTrimLength(70);
        mPlotText.setText(entityFull.getPlot());
        mRatingBar.setNumStars(10);
        mRatingBar.setMax(10);
        try {
            mRatingBar.setRating(Float.parseFloat(entityFull.getImdbRating()));
        } catch (NumberFormatException e) {
            mRatingBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<MovieEntityFull> loader) {

    }

    @Override
    public void onNetworkStateChanged(boolean mNetAvailable) {
        mNoInternetView.setVisibility(mNetAvailable ? View.GONE : View.VISIBLE);
        mMovieView.setVisibility(mNetAvailable ? View.VISIBLE : View.GONE);
    }
}
