package shander.imdbparser.ui.fragment;

import android.app.Fragment;

public abstract class BaseFragment extends Fragment {

    boolean mIsNetworkAvailable;

    public void onNetworkStateChanged(boolean mNetAvailable) {
        mIsNetworkAvailable = mNetAvailable;
    }

}
