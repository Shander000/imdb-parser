package shander.imdbparser.ui.fragment;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import shander.imdbparser.R;
import shander.imdbparser.entities.MovieEntity;
import shander.imdbparser.entities.Search;
import shander.imdbparser.ui.adapter.MoviesAdapter;
import shander.imdbparser.utils.loader.MoviesLoader;

public class SearchFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Search>{

    private OnMovieSelectedListener mListener;
    private boolean mIsNetworkAvailable;
    private Set<MovieEntity> mMovies = new LinkedHashSet<>();
    private Loader<Search> mLoader;
    private String mSearchString = "";
    private MoviesAdapter mAdapter;
    private int mPage = 1;
    private int mResults;

    @BindView(R.id.search_et)
    TextInputEditText mSearchInput;

    @BindView(R.id.search_btn)
    Button mSearchButton;

    @BindView(R.id.list_error_tv)
    TextView mErrorText;

    @BindView(R.id.result_rv)
    RecyclerView mResultsRecycler;

    @BindView(R.id.no_internet_view)
    View mNoInternetView;

    @BindView(R.id.settings_btn)
    Button mSettings;

    public SearchFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSettings.setOnClickListener(view -> startActivity(new Intent(Settings.ACTION_SETTINGS)));
        mSearchButton.setOnClickListener(v -> {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            if (TextUtils.isEmpty(mSearchInput.getText())) {
                mSearchInput.setError(getString(R.string.no_search_text));
            } else {
                mMovies.clear();
                mAdapter.notifyDataSetChanged();
                mPage = 1;
                mSearchString = mSearchInput.getText().toString();
                if (mIsNetworkAvailable) {
                    if (mLoader == null) {
                        mLoader = getActivity().getLoaderManager().initLoader(1, null, this);
                    } else {
                        getActivity().getLoaderManager().restartLoader(1, null, this);
                    }
                }
            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        mResultsRecycler.setLayoutManager(llm);
        mAdapter = new MoviesAdapter(this);
        mAdapter.setOnBottomReachedListener(position -> {
            if (mPage < mResults/10) {
                mPage++;
                if (mLoader == null) {
                    mLoader = getActivity().getLoaderManager().initLoader(1, null, this);
                } else {
                    getActivity().getLoaderManager().restartLoader(1, null, this);
                }
            }
        });
        DividerItemDecoration decoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL);
        mResultsRecycler.setAdapter(mAdapter);
        mResultsRecycler.addItemDecoration(decoration);
        if (!TextUtils.isEmpty(mSearchString)) {
            mSearchInput.setText(mSearchString);
        }
        if (mMovies != null && mMovies.size() != 0) {
            mAdapter.setMoviesItems(new ArrayList<>(mMovies));
        }
    }

    public void onMovieSelected(String id) {
        if (mListener != null) {
            mListener.onMovieSelected(id);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onNetworkStateChanged(mIsNetworkAvailable);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMovieSelectedListener) {
            mListener = (OnMovieSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @NonNull
    @Override
    public Loader<Search> onCreateLoader(int id, @Nullable Bundle args) {
        mLoader = new MoviesLoader(getActivity(), mSearchString, mPage);
        return mLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Search> loader, Search data) {
        if (data.getResponse().equals("True")) {
            mResults = Integer.parseInt(data.getTotalResults());
            mMovies.addAll(data.getSearch());
            mAdapter.setMoviesItems(new ArrayList<>(mMovies));
            if (mListener != null) {
                mListener.onMoviesLoaded(mMovies, mSearchString);
            }
            mResultsRecycler.setVisibility(View.VISIBLE);
            mErrorText.setVisibility(View.GONE);
        } else {
            showError(data.getError());
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Search> loader) {

    }

    private void showError(String error) {
        mMovies.clear();
        mAdapter.notifyDataSetChanged();
        mPage = 1;
        mResultsRecycler.setVisibility(View.GONE);
        mErrorText.setVisibility(View.VISIBLE);
        mErrorText.setText(getResources().getString(R.string.error, error));
    }

    public interface OnMovieSelectedListener {
        void onMovieSelected(String id);
        void onMoviesLoaded(Set<MovieEntity> mMovies, String search);
    }

    @Override
    public void onNetworkStateChanged(boolean mNetAvailable) {
        mIsNetworkAvailable = mNetAvailable;
        mNoInternetView.setVisibility(mNetAvailable ? View.GONE : View.VISIBLE);
        mResultsRecycler.setVisibility(mNetAvailable ? View.VISIBLE : View.GONE);
    }

    public void setMovies(ArrayList<MovieEntity> movies, String search) {
        mMovies.addAll(movies);
        mSearchString = search;
    }
}
