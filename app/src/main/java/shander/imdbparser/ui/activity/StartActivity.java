package shander.imdbparser.ui.activity;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Set;

import shander.imdbparser.R;
import shander.imdbparser.entities.MovieEntity;
import shander.imdbparser.ui.fragment.BaseFragment;
import shander.imdbparser.ui.fragment.MovieDetailsFragment;
import shander.imdbparser.ui.fragment.SearchFragment;
import shander.imdbparser.utils.ServiceManager;

public class StartActivity extends AppCompatActivity implements SearchFragment.OnMovieSelectedListener{

    boolean mIsNetworkAvailable;
    private FragmentTransaction mFragmentTrans;
    private SearchFragment mSearchFragment;
    private ArrayList<MovieEntity> mMoviesList = new ArrayList<>();
    private String mOldSearch;

    private BroadcastReceiver networkStateReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = manager.getActiveNetworkInfo();
            mIsNetworkAvailable = ni != null && ni.isConnectedOrConnecting();
            onNetworkStateChanged(mIsNetworkAvailable);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ServiceManager mManager = new ServiceManager(this);
        mIsNetworkAvailable = mManager.isNetworkAvailable();
        onNetworkStateChanged(mIsNetworkAvailable);
        mSearchFragment = new SearchFragment();
        mFragmentTrans = getFragmentManager().beginTransaction();
        mFragmentTrans.replace(R.id.fragment_lay, mSearchFragment, "SEARCH_FRAGM").commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        unregisterReceiver(networkStateReceiver);
        super.onPause();
    }

    private void onNetworkStateChanged(boolean mNetAvailable){
        if (getFragmentManager().findFragmentById(R.id.fragment_lay) != null) {
            ((BaseFragment) (getFragmentManager().findFragmentById(R.id.fragment_lay))).onNetworkStateChanged(mNetAvailable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onMovieSelected(String id) {
        mFragmentTrans = getFragmentManager().beginTransaction();
        mFragmentTrans.replace(R.id.fragment_lay, MovieDetailsFragment.newInstance(id), "MOVIE_DETAILS").commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onMoviesLoaded(Set<MovieEntity> mMovies, String search) {
        mMoviesList.addAll(mMovies);
        mOldSearch = search;
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().findFragmentById(R.id.fragment_lay) instanceof MovieDetailsFragment) {
            if (mSearchFragment == null) {
                mSearchFragment = new SearchFragment();
                if (mMoviesList != null && mMoviesList.size() != 0) {
                    mSearchFragment.setMovies(mMoviesList, mOldSearch);
                }
            }
            mFragmentTrans = getFragmentManager().beginTransaction();
            mFragmentTrans.replace(R.id.fragment_lay, mSearchFragment).commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else {
            finish();
        }
    }
}
