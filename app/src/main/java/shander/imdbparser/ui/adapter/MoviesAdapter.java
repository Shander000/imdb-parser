package shander.imdbparser.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import shander.imdbparser.R;
import shander.imdbparser.entities.MovieEntity;
import shander.imdbparser.ui.fragment.SearchFragment;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ItemHolder>{

    private LayoutInflater mLayoutInflater;
    private SearchFragment mFragment;
    private List<MovieEntity> mMoviesList;
    private OnBottomReachedListener mOnBottomReachedListener;

    public MoviesAdapter(SearchFragment fragment) {
        this.mFragment = fragment;
        mLayoutInflater = LayoutInflater.from(fragment.getActivity());
        mMoviesList = new ArrayList<>();
    }

    public void setMoviesItems(List<MovieEntity> items) {
        this.mMoviesList = items;
        notifyDataSetChanged();
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){
        this.mOnBottomReachedListener = onBottomReachedListener;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemHolder(mLayoutInflater.inflate(R.layout.movie_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        final MovieEntity currentItem = mMoviesList.get(position);
        holder.setItem(currentItem);
        holder.itemView.setOnClickListener(view -> mFragment.onMovieSelected(currentItem.getImdbID()));
        if (position == mMoviesList.size() - 1){
            mOnBottomReachedListener.onBottomReached(position);
        }
    }

    @Override
    public int getItemCount() {
        return mMoviesList.size();
    }

    class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumb_iv)
        ImageView itemThumb;

        @BindView(R.id.item_title_tv)
        TextView itemText;

        ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setItem(MovieEntity item) {
            Picasso.get().load(item.getPoster()).resize(60, 60)
                    .centerCrop().placeholder(R.drawable.no_image).into(itemThumb);
            itemText.setText(item.getTitle());
        }
    }

    public interface OnBottomReachedListener {

        void onBottomReached(int position);

    }
}
