package shander.imdbparser.utils.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;

import shander.imdbparser.entities.Search;

import static shander.imdbparser.utils.IContract.API_KEY;
import static shander.imdbparser.utils.IContract.BASE_URL;
import static shander.imdbparser.utils.IContract.PAGE;
import static shander.imdbparser.utils.IContract.SEARCH;

public class MoviesLoader extends AsyncTaskLoader<Search> {

    private String mSearchString;
    private int mPage;

    public MoviesLoader(@NonNull Context context, String search, int page) {
        super(context);
        mSearchString = search;
        mPage = page;
    }

    @Override
    public void forceLoad() {
        super.forceLoad();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void deliverResult(Search data) {
        super.deliverResult(data);
    }

    @Nullable
    @Override
    public Search loadInBackground() {
        Search search;
        try {
            String json = IOUtils.toString(new URL(BASE_URL + SEARCH + mSearchString + PAGE + String.valueOf(mPage) + API_KEY));
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            search = gson.fromJson(json, Search.class);
            if (search != null) {
                return search;
            }
        } catch (IOException e) {
            Search search1 = new Search();
            search1.setResponse("False");
            search1.setError("Internal app error occurs");
            return search1;
        }
        return null;
    }
}
