package shander.imdbparser.utils.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;

import shander.imdbparser.entities.MovieEntityFull;

import static shander.imdbparser.utils.IContract.API_KEY;
import static shander.imdbparser.utils.IContract.BASE_URL;
import static shander.imdbparser.utils.IContract.FULL_INFO;

public class FullMovieInfoLoader extends AsyncTaskLoader<MovieEntityFull> {

    private String mIMDBid;

    public FullMovieInfoLoader(@NonNull Context context, String id) {
        super(context);
        mIMDBid = id;
    }

    @Override
    public void forceLoad() {
        super.forceLoad();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void deliverResult(MovieEntityFull data) {
        super.deliverResult(data);
    }

    @Nullable
    @Override
    public MovieEntityFull loadInBackground() {
        MovieEntityFull movie;
        try {
            String json = IOUtils.toString(new URL(BASE_URL + FULL_INFO + mIMDBid + API_KEY));
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            movie = gson.fromJson(json, MovieEntityFull.class);
            if (movie != null) {
                return movie;
            }
        } catch (IOException e) {
            MovieEntityFull movie1 = new MovieEntityFull();
            movie1.setResponse("False");
            movie1.setError("Internal app error occurs");
            return movie1;
        }
        return null;
    }
}
