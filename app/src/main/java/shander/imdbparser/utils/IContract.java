package shander.imdbparser.utils;

public interface IContract {

    String BASE_URL = "http://www.omdbapi.com/";
    String SEARCH = "?s=";
    String PAGE = "&page=";
    String FULL_INFO = "?i=";
    String API_KEY = "&apikey=be71648c";
}
