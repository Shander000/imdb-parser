package shander.imdbparser.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Search {

    @SerializedName("Search")
    private List<MovieEntity> search = null;
    @SerializedName("totalResults")
    private String totalResults;
    @SerializedName("Response")
    private String response;
    @SerializedName("Error")
    private String error;

    public List<MovieEntity> getSearch() {
        return search;
    }

    public void setSearch(List<MovieEntity> search) {
        this.search = search;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
